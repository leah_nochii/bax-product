import React from 'react';
import  style from "../../style/main.less";


const ProductReviews =({reviews,questions})=>{
    //lets get the reviews and sort them by latest dates
    const reviewList =  reviews.length ? (reviews.sort((a, b) => new Date( a.reviewdate ) > new Date( b.reviewdate )).map((review,i )=>{
        return ( 
            <div className={style.reviewlist} key={review.id}>
                 <span className={style.name}><b>{review.firstname+' '+review.lastname.substring(0,1)+'.'}</b></span>
                 <span className={style.rating}><ul className={style.stars + ' ' + style.stars0}> </ul></span>
                 <span className={style.rate}>Rating : {review.rate}</span>
                 <p><span>{new Date(review.reviewdate).toLocaleDateString('en-US') }</span></p>
                 <p>{review.description}</p>
            </div>

        )
    }))  : (<h4>No question yet.</h4>)
    //just for the question -- nothing in here
    const questionList =  questions.length ? (questions.map(question =>{
                            return (
                                <div className="review-data-list" key={question.id}>
                                   
                                </div>

                            )
                        }))  : (<h4>No question yet.</h4>)

    return(
        
        <div className={style.reviewcontainer}>
            <ul className={'nav nav-tabs ' + style.navtabs} id="interactionTab" role="tablist">
            <li className={'nav-item ' +  style.navitem}>
                <a className={'nav-link ' +  style.navlink + ' active'} id="reviews-tab" data-toggle="tab" href="#reviews" role="tab" aria-controls="reviews" aria-selected="true">REVIEWS</a>
            </li>
            <li className={'nav-item ' +  style.navitem}>
                <a className={'nav-link ' +  style.navlink} id="question-tab" data-toggle="tab" href="#question" role="tab" aria-controls="question" aria-selected="false">QUESTIONS</a>
            </li>
            </ul>
            <div className="tab-content" id="interactionTabContent">
                <div className="tab-pane fade show active __list" id="reviews" role="tabpanel" aria-labelledby="reviews-tab">{reviewList}</div>
                <div className="tab-pane fade" id="question" role="tabpanel" aria-labelledby="question-tab"></div>
            </div>
        </div>
    )
    
}
export default ProductReviews;