import React, { Component } from 'react';
import  style from "../../style/main.less";

class ProductReviewForm extends Component{
    state = {
        description: '',
        firstname: '',
        rate: '',
        reviewdate : '',
        lastname : '',
        email : '',
        errors : {}
        ,
        showreview : false
    }
    
    handleReviews =(e)=> {

        e.preventDefault();
        //lets revalidate the fields
        Object.keys(this.state).map(
          (target,i) => { 
              if(document.getElementsByName(target).length > 0 ){
                
                this.handleValidation(target,document.getElementsByName(target)[0].value)
              }
              
            })
            console.log(Object.keys(this.state.errors).length)
        //if no error continue adding the review in the state reviews
        if(Object.keys(this.state.errors).length === 0 ){
            this.state.reviewdate =  new Date().getTime();
            this.props.newReview(this.state);
            this.setState({ description: '',
                        firstname: '',
                        rate: '0',
                        lastname : '',
                        email : '',
                        errors : {},
                        reviewdate : '',
                        showreview : false,
                        success : 1
                });
        }else{
            
        }
 
    }

    handleInputs = (e) => {
        //adding the field to appropriate state variable
        this.setState({
            [e.target.name]: e.target.value
        })
        this.handleValidation(e.target.name,e.target.value);
    }
   
    handleValidation (target,value){
        let fieldErrors = '';
        switch(target) {
            case 'email':
              let emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
              fieldErrors = emailValid ? '' : 'Invalid Email' ;
              break;
            case 'description':
                fieldErrors = value.length  >= 10 ? '' : 'Max 10 characters required';
                break;
            case 'rate':
                fieldErrors =(value <= 5  && value >=1)  ? '' : 'Rating scale must in numbers 1-5';
                console.log(fieldErrors)
                break;
            case 'firstname':
              fieldErrors = value.length <=0 || value==='' ? 'Field ' +target +' is required' : '';   //let make all fiel as required
              break;
            case 'lastname':
              fieldErrors = value.length <=0 || value==='' ? 'Field ' +target +' is required' : '';   //let make all fiel as required
              break;
          }
          let errors = this.state.errors;
          if(fieldErrors){
            errors[target] = fieldErrors;
            this.setState({errors : errors});
          }else{
            let newerrors = {};
            for(var index in errors){
                if(index !==target){
                    newerrors[index] =  errors[index]
                }
              }
            
              this.setState({errors : newerrors});
              console.log(this.state.errors)
          }
    }
    handleFormView =(e)=>{
        //this handle the displaying of review form
        this.setState({ showreview: true ,success : ''});
    }
    render(){
           
        return(
            <div className="reviewfrom">
                <div className={style.actionbuttons}>
                    <button className={style.btnc} onClick={this.handleFormView} >Write A Review</button>
                    <button className={style.btnc}>Ask Question</button>
                </div>
                <div className={style.notificationcontainer}>
                {this.state.success ? <div className="alert alert-success">Thank you for sending us your review</div> : ''}
                
                </div>

                <div className={style.formContainer + ' ' + '_show'+this.state.showreview}  >
                
                <form className="form-horizontal" onSubmit={this.handleReviews} >
                    
                    <div className="form-group">
                        <label className="control-label col-sm-2">Firstname:</label>
                        <div className="col-sm-10">
                            {this.state.errors.firstname ? <div  className="alert alert-danger">{this.state.errors.firstname}</div> : ''}
                            <input type="text" value={this.state.firstname} className="form-control"  name='firstname' onChange={this.handleInputs} />
                        
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="control-label col-sm-2">Lastname:</label>
                        <div className="col-sm-10">
                             {this.state.errors.lastname ? <div  className="alert alert-danger">{this.state.errors.lastname}</div> : ''}
                            <input type="text"value={this.state.lastname} className="form-control" name='lastname' onChange={this.handleInputs} />
                        
                        </div>
                    </div>         
                    <div className="form-group">
                        <label className="control-label col-sm-2">Email:</label>
                        <div className="col-sm-10">
                            {this.state.errors.email ? <div  className="alert alert-danger">{this.state.errors.email}</div> : ''}
                            <input type="email" value={this.state.email} className="form-control" name='email' onChange={this.handleInputs}  />
                            
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2">Rating:</label>
                        <div className="col-sm-10">
                        {this.state.errors.rate ? <div  className="alert alert-danger">{this.state.errors.rate}</div> : ''}
                            <select  name="rate" className="form-control"  onChange={this.handleInputs} >
                                {this.state.rate == 0 ?  <option value="0" selected>Select</option> : <option value="0" >Select</option>}
                               
                                <option value="5">5</option>
                                <option value="4">4</option>
                                <option value="3">3</option>
                                <option value="2">2</option>
                                <option value="1">1</option>
                            </select>
                          
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" >Description:</label>
                        <div className="col-sm-10">
                            {this.state.errors.description ? <div  className="alert alert-danger">{this.state.errors.description}</div> : ''}
                            <textarea name="description" value={this.state.description} className="form-control"  onChange={this.handleInputs} ></textarea>
                          
                        </div>
                    </div>
                    <button type="submit" className={style.actionbuttons} className={style.btnc} >Submit</button>
                </form>

                </div>
            </div>
        )

    }
}
export default ProductReviewForm;