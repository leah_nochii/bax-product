import React from 'react';
import  style from "../../style/main.less";
import logo from '../../images/devine_logo.png';
const ProductMain =()=>{
    //nothing in here - supposed to be the renderer of the product page - product info
    const productInfo = (
        <div className={style.__info}>
            <div className={style.logo}>
            <img src={logo}></img>
            <span>DEVINE</span>
            </div>
            <div className={style.title}><h3>Devine Artis 15A 400W active speaker</h3></div>
        </div>
    )

    return(
            <section className={'container-fluid ' +style.product}>
                <div className={style.productinfo}>
                    <div className={style.__gallery}>
                        
                    </div>
                    {productInfo}
                </div>
            </section>
    )
    
}
export default ProductMain;