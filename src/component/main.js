import React, {Component} from 'react';
import ProductMain from './sections/productMain';
import ProductReview from './sections/productReview';
import ProductReviewForm from './sections/productReviewForm';

import  style from "../style/main.less";
import { throws } from 'assert';
const imgDir = '../images';
class ProductPage extends Component {
    state = {
        reviews : [
            { id: 10, reviewdate:1526451027000, firstname : 'Ora',lastname : 'Winslow', rate : 3.5, description : "I STRONGLY recommend Devine Artis speaker to EVERYONE interested in running a successful online business! Devine Artis speaker was the best investment I ever made. We've used Devine Artis speaker for the last five years. I STRONGLY recommend Devine Artis speaker to EVERYONE interested in running a successful online business!", photo : imgDir+'/profile.png'},
            { id: 9, reviewdate:1526364627000,firstname : 'Carren',lastname : 'Stan', rate : 5, description : "Best. Product. Ever! If you want real marketing that works and effective implementation - Devine Artis speaker's got you covered. I could probably go into sales for you.", photo : imgDir+'/profile.png'},
            { id: 8, reviewdate:1526364626000,firstname : 'Archie',lastname : 'Grey', rate : 5, description : "We're loving it. The best on the net! Devine is awesome! Since I invested in Devine I made over 100,000 dollars profits.", photo : imgDir+'/profile.png'},
            { id: 7, reviewdate:1504095597183,firstname : 'Esha',lastname : 'Hodges', rate : 4, description : "The best on the net! It's just amazing.", photo : imgDir+'/profile.png'},
            { id: 6, reviewdate:1504095567183,firstname : 'Garin',lastname : 'Sheridan', rate : 4, description : "I like Devine more and more each day because it makes my life a lot easier.", photo : imgDir+'/profile.png'},
            { id: 5, reviewdate:1504095567183,firstname : 'Ishaan',lastname : 'Sanchez', rate : 4, description : "The best on the net! Devine saved my business. It's the perfect solution for our business. I'm good to go.", photo : imgDir+'/profile.png'},
            { id: 4, reviewdate:1504095567183,firstname : 'Deon',lastname : 'Croft', rate : 5, description : "We were treated like royalty. Devine impressed me on multiple levels. No matter where you go, Devine is the coolest, most happening thing around!", photo : imgDir+'/profile.png'},
            { id: 3, reviewdate:1504095567183,firstname : 'Magnus',lastname : 'Rutledge', rate : 2, description : "I would be lost without Devine. Nice work on your Devine. Very easy to use. I would be lost without Devine.", photo : imgDir+'/profile.png'},
            { id: 2, reviewdate:1504095567183,firstname : 'Joanne',lastname : 'Hahn', rate : 5, description : " Devine is the real deal!", photo : imgDir+'/profile.png'},
            { id: 1, reviewdate:1504095567183,firstname : 'Kristian',lastname : 'Melia', rate : '1', description : "I will refer everyone I know. Devine has completely surpassed our expectations. The very best. We've used Devine for the last five years.", photo : imgDir+'/profile.png'},
        ],
        questions : [
            { id: 1}
        ],
        totalratings : '4.0',
        currentpage : 1,
        reviewperpage : 3
        
    }
    newReview = (review)=>{
        review.id =  this.state.reviews.length + 1
        let reviews = [review,...this.state.reviews]
        this.setState({
            reviews
        })
    }
    handlePagination=(e)=>{
        if(e.target.id !=''){
            this.setState({
                currentpage: Number(e.target.id)
              })
        }
  
    }
    render() {
        const data = this.state.reviews;
        const reviewTotal =Number (((this.state.reviews.reduce((totalReviews, review) => totalReviews + parseInt(review.rate), 0)) / this.state.reviews.length ).toFixed(1));
        //lets prepare the pagination dependencies
        const indexOfLastData = this.state.currentpage * this.state.reviewperpage;
        const indexOfFirstData = indexOfLastData - this.state.reviewperpage;
        const paginateddata = data.slice(indexOfFirstData, indexOfLastData);

        const pageNumbers = [];
          for (let i = 1; i <= Math.ceil(this.state.reviews.length / this.state.reviewperpage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            return (
              <li key={number}  id={number} onClick={this.handlePagination} className={style.pgoptions + ' '+(this.state.currentpage === number ? style.pgactive : '')}>
                {number}
              </li>
            );
        });
        return(
            <div className={style.productcontainer}>
                <section className={'container ' + style.reviews}>
                    <div className={style.__summary}> 
                        <span className={style.__rating}>{reviewTotal}
                        <ul className={style.stars + ' ' + style.stars0}>
                            </ul>
                        </span>
                        
                        <span className={style.__comments}>{this.state.reviews.length} Reviews</span>
                    </div>
                    <ProductReviewForm newReview={this.newReview}/> 
                    <ProductReview reviews={paginateddata}   questions={this.state.questions}/>

                    <div className={style.paginationcontainer}>
                        <ul className={style.pagination}>
                            <li  className={style.pgoptions}><i className="fa fa-chevron-left"></i></li>
                            {renderPageNumbers}
                            <li  className={style.pgoptions}><i className="fa fa-chevron-right"></i></li>
                        </ul>
                    </div>

                </section>
            </div>

        );
    }
}
export default ProductPage;
