# Bax Product Review Test Case Exam
Below are some key pieces of information for deploying the project. If any issues/problems are encountered during the deployment, kindly reach me through my email leah@codingchiefs.com.

Thank you. 

### Dependencies

Install frontend dependencies with [NPM](https://www.npmjs.com/). Open a command line tool and execute the following command within the root of your clone: 
```
$ npm install
```
### Development
 Compile the codebase for development/deployment. Open a command line tool and execute any of the following command within the root of your clone: 
```
$ npm start
```
or
```
$ npm run dev
```

### Site Address
Once successfully compiled, please look at the compilation logs for the address where the project is running. In most cases, when not in use, the project will be served on:
```
http://localhost:8080
```

### About the Assessment
When you open the project,you should see a review section that displays the average review, total review, a button to display the review form, and a list of reviews with the following details: name, user rate, date of the review, and the description. 

As per the test instructions, we should not display the email address of the reviewer. To check if the added review data are correct, you can use the [Chrome extension React](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en) to confirm the review's data object. Once the extension is enabled, go to the Chrome inspector and look for the Reac tab. You should see a list of state objects under the element <ProductPage>. One of these is the Reviews object. 

Below are the following frameworks/technologies I used for the project: 

* [Bootstrap](https://getbootstrap.com/)
* [LESS](http://lesscss.org/)
* [React](https://reactjs.org/)
* [Webpack](https://webpack.js.org/)
 
For other modules that have been used, you may check the project file package.json.